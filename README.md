# 公信流媒体服务器

## 介绍
私有协议实现的流媒体服务器，用于局域网内多台设备实现直播，将视频统一编码成720P再逐个终端进行推送。视频采用H264，音频采用AAC。

## 软件架构
使用Netty实现私有协议，使用bytedeco ffmpeg 编解码视频流

## 环境要求

- apache maven 3.6.2
- java jdk 1.8

## 打包流程

~~~ cmd
mvn clean compile package
~~~

将生成的 media-server-1.0-SNAPSHOT.jar 复制到bin目录下，双击run.bat即可


## 下一步计划
- [ ] 实现rtsp服务器
- [ ] 将本地的流，推送到rtsp服务器
- [ ] 实现分布式部署