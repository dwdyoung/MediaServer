rem -XX:+UseParNewGC
java -Xms1024m -Xmx1024m -XX:+UseConcMarkSweepGC -Dio.netty.leakDetectionLevel=advanced -Xloggc:gc.log -XX:MaxDirectMemorySize=1G -Dio.netty.allocator.pageSize=8192 -Dio.netty.allocator.maxOrder=10 -Dio.netty.recycler.maxCapacity=0 -Dio.netty.recycler.maxCapacity.default=0 -jar media-server-1.0-SNAPSHOT.jar
