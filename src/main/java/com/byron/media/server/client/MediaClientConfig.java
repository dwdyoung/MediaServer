package com.byron.media.server.client;

public class MediaClientConfig {

    private byte serverId;

    private String serverAddress;

    private int serverPort;

    private int heartTimeout = 30;       // 心跳超时时间

    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public int getHeartTimeout() {
        return heartTimeout;
    }

    public void setHeartTimeout(int heartTimeout) {
        this.heartTimeout = heartTimeout;
    }

    public byte getServerId() {
        return serverId;
    }

    public void setServerId(byte serverId) {
        this.serverId = serverId;
    }
}
