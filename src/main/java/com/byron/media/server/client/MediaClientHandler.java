package com.byron.media.server.client;

import com.byron.media.server.enums.Args;
import com.byron.media.server.enums.MediaType;
import com.byron.media.server.enums.ProxyType;
import com.byron.media.server.sessions.*;
import com.byron.media.server.logic.HandlerContainer;
import com.byron.media.server.model.MediaData;
import com.byron.media.server.model.MediaHeaderObj;
import com.byron.media.server.model.ProxyMediaData;
import com.byron.media.server.utils.JsonUtils;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;

@Slf4j
@ChannelHandler.Sharable
public class MediaClientHandler extends ChannelInboundHandlerAdapter {

    private MediaClientConfig config;

    public MediaClientHandler(MediaClientConfig config) {
        this.config = config;
    }


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {

        // 连上之后发送组名
//        ctx.writeAndFlush(mediaConfig.getGroup());

        RealSession session = ctx.pipeline().get(RealSession.class);

        MediaHeaderObj header = new MediaHeaderObj();
        header.getCmd().put("cmd", "reg");
        header.getCmd().put("respone", 0);
//        header.getArgs().put(Args.GROUP_NAME, mediaConfig.getGroup());
        header.getArgs().put(Args.PROXY, true);
        header.getArgs().put(Args.SERVER_ID, config.getServerId());
//        header.getDevice().put("id", mediaConfig.getDevice());

        String json = JsonUtils.toJson(header);
        ctx.writeAndFlush(json);

        IProxyClient proxyClient = new ProxyClient(session);
        proxyClient.active();

    }


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        RealSession realSession = ctx.pipeline().get(RealSession.class);

        // 收到来自服务器的代理媒体数据
        if(msg instanceof ProxyMediaData){
            ProxyMediaData proxy = (ProxyMediaData)msg;
            if(proxy.getProxyType() == ProxyType.MEDIA_DATA){
                Group group = SessionContainer.getInstance().getGroup(proxy.getGroupName());
                group.sendToOthers(proxy.getLastSeverId(), proxy.getSessionName(), proxy.getMediaData());

//                Group group = SessionContainer.getInstance().getGroup(proxy.getGroupName());
//                if(group != null){
//                    group.writeExcept(proxy.getServerId(), proxy.getMediaData());
//                }

//                session.write(proxy.getGroupName(), proxy.getMediaData());
//                session.writeExcept(proxy.getServerId(), proxy.getGroupName(), proxy.getMediaData());
//                if(session.isActive()){
//                    session.sendToAll(mediaData);
//                }
            } else if(proxy.getProxyType() == ProxyType.JSON){
                // 设备上线或离线消息
//                log.error("代理客户端，不会收到json数据");
                MediaHeaderObj header = JsonUtils.fromJson(proxy.getJson(), MediaHeaderObj.class);

                String cmd = (String) header.getCmd().get("cmd");
                if(cmd == null){
                    return;
                }

                cmd = "proxy-" + cmd;

                HandlerContainer.getInstance().handle(cmd, realSession, ctx, header);
            }

        // TODO 理论上不会收到来自服务器的媒体数据
        } else if(msg instanceof MediaData){
            MediaData media = (MediaData)msg;
            if(MediaType.isHeart(media.getType())){
                ctx.fireChannelReadComplete();
                return;
            }
            Assert.assertTrue("理论上不会收到来自服务器的媒体数据", false);
//        } else if(msg instanceof String){
//            String json = (String) msg;
//            MediaHeaderObj header = JsonUtils.fromJson(json, MediaHeaderObj.class);
//
//            if(header.getCmd() == null){
//                return;
//            }
//
//            String cmd = (String) header.getCmd().get("cmd");
//            if(cmd == null){
//                return;
//            }
//
//            if(cmd.equals("unreg")){
//                String groupName = (String) header.getArgs().getOrDefault(Args.GROUP_NAME, "");
//                if("".equals(groupName)){
//                    return;
//                }
//                boolean sender = (boolean) header.getArgs().getOrDefault(Args.SENDER, false);
//                String device = (String) header.getDevice().get("id");
//                log.info("主服务器客户端 " + device + "断开连接");
//                if(sender){
//                    sessionContainer.cleanIframe(groupName);
//                }
//            }
        }
        ctx.fireChannelReadComplete();
    }
}
