package com.byron.media.server.config;

import com.byron.media.server.MediaServer;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class MediaServerConfig {

    private byte serverId;              // 本机的id，必须唯一

    private int port;                   // 监听端口

    private String rootFilePath;        // 本地文件目录

    private List<String> serverHosts = new ArrayList<>();   // 集群的其他服务器ip和端口，格式为  ip:port

    private int heart;                  // 心跳

    private int process;            // 线程数

    private int maxMills;            // 当已经收到最大毫秒数为max时候，暂停拉取

    private int minMills;            // 当已经收到最小毫秒数为min时候，就继续拉取

    private String mainServer = "192.168.1.186";

    private String serverMode = "singleton";

    private int mainPort = 1553;

    /**
     * 是否打印日志
     */
    private boolean log;

    private int messagePort;

    public String getMainServer() {
        return mainServer;
    }

    public void setMainServer(String mainServer) {
        this.mainServer = mainServer;
    }

    public int getMessagePort() {
        return messagePort;
    }

    public void setMessagePort(int messagePort) {
        this.messagePort = messagePort;
    }

    private MediaServerConfig(){}

    private static MediaServerConfig instance;
    public static MediaServerConfig getInstance(){
        if(instance == null){
            instance = new MediaServerConfig();
        }
        return instance;
    }
}
