package com.byron.media.server.enums;

public class Args {

    public static final String GROUP_NAME = "group_name";
//    public static final String SENDER = "sender";
    public static final String TYPE = "type";
    public static final String PROXY = "proxy";
    public static final String SESSION_NAME = "session_name";       // 连接的名字，32位随机字符串
    public static final String SERVER_ID = "server_id"; // 代理服务器自身的serverId，每个服务器的serverId必须是唯一
    public static final String FILE = "file";       // 播放本地文件时需要此参数
    public static final String HTTP = "";           // 拉取http流媒体时需要此参数
    public static final String RTSP = "";           // 拉取RTSP流的时候需要此参数
    public static final String RTMP = "";           // 拉取RTMP的时候需要此参数

}
