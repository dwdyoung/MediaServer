package com.byron.media.server.enums;

public class ProxyType {

    public static final int JSON = 0;           // 代理的数据是json
    public static final int MEDIA_DATA = 1;     // 代理的数据是mediaData

}
