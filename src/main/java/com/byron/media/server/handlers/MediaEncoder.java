package com.byron.media.server.handlers;

import com.byron.media.server.enums.MediaType;
import com.byron.media.server.enums.ProxyType;
import com.byron.media.server.model.MediaData;
import com.byron.media.server.model.ProxyMediaData;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;

@Slf4j
//@ChannelHandler.Sharable
public class MediaEncoder extends ChannelOutboundHandlerAdapter {

    private long counter;

    @Override
    public void write(ChannelHandlerContext ctx, Object message, ChannelPromise promise) throws Exception {
        counter++;
        if(message instanceof MediaData){
            MediaData data = (MediaData)message;
            ByteBuf buf = ctx.alloc().buffer(data.getLength() + 4);
            buf.writeByte((byte)'$');   // $ 开始符  0
            buf.writeByte(data.getChannel());     // 通道号 1
            buf.writeByte(data.getType());     // 类型 2
//            buf.writeLong(data.getID());           //
            buf.writeInt(data.getID());           // 单元号 3\4\5\6
            buf.writeByte(data.getSeq());       // 包名 7
            buf.writeByte(data.getServerId());  // 服务id 8
//            buf.writeByte(((data.getSeq() << 4) & 0xFF00) | (data.getServerId() & 0xFF));
//            buf.writeByte(0);               // 无用
            buf.writeShort(0);              // 无用  9\10
//            buf.writeInt(data.getSeq());           // 包名和服务器id

            buf.writeInt(data.getLength());  // 数据长度  11\12\13\14
            buf.writeBytes(data.getData(), 0, data.getLength());       // 数据
            ctx.writeAndFlush(buf, promise);
            return;
        }

        if(message instanceof ProxyMediaData){

            ProxyMediaData proxyData = (ProxyMediaData)message;
            int proxyType = proxyData.getProxyType();
            if(proxyType == ProxyType.MEDIA_DATA){
                if(proxyData.getMediaData() == null){
                    return;
                }
                byte[] groupNameBytes = proxyData.getGroupName().getBytes(StandardCharsets.UTF_8);      // 字符串长度
                ByteBuf buf = ctx.alloc().buffer(proxyData.getMediaData().getLength() + 5 + 2 + groupNameBytes.length + 15);
                buf.writeByte((byte)'$');   // $ 开始符
                buf.writeByte(proxyData.getMediaData().getChannel());     // 通道号
                byte type = 0;
                type = MediaType.putProxy(type);
                type = MediaType.putStart(type);
                type = MediaType.putEnd(type);
                buf.writeByte(type);                                    // 类型（代理）
                MediaData data = proxyData.getMediaData();
                buf.writeInt(data.getID());           // 单元号

                buf.writeByte(data.getSeq());       // 包名
                buf.writeByte(data.getServerId());  // 服务id
//                buf.writeByte(((data.getSeq() << 4) & 0xFF00) | (data.getServerId() & 0xFF));
//                buf.writeByte(0);               // 无用
                buf.writeShort(0);              // 无用
                buf.writeInt(16 + groupNameBytes.length + data.getLength());     // 数据总长度

                // 以下是代理数据
                buf.writeByte(ProxyType.MEDIA_DATA);            // 代理类型
                buf.writeLong(proxyData.getSessionName());      // 连接的别名
                buf.writeShort(groupNameBytes.length);          // 组字符串的长度
                buf.writeBytes(groupNameBytes);                 // 组字符串的实体

                buf.writeByte(data.getType());                  // 数据的实际类型
                buf.writeInt(data.getLength());                 // 数据实际长度
                buf.writeBytes(data.getData(), 0, data.getLength());       // 原数据内容
                ctx.writeAndFlush(buf, promise);
            } else if(proxyType == ProxyType.JSON){
                if(proxyData.getJson() == null){
                    return;
                }
                byte[] jsonBytes = proxyData.getJson().getBytes(StandardCharsets.UTF_8);      // 字符串长度
                ByteBuf buf = ctx.alloc().buffer(5 + 2 + jsonBytes.length + 15);
                buf.writeByte((byte)'$');   // $ 开始符
                buf.writeByte(0);           // 通道号
                byte type = 0;
                type = MediaType.putProxy(type);
                type = MediaType.putEnd(type);
                type = MediaType.putStart(type);
                buf.writeByte(type);                             // 类型（代理）
                buf.writeInt(0);           // 单元号
//                buf.writeByte(((proxyData.getSeq() << 4) & 0xFF00) | (proxyData.getServerId() & 0xFF));
                buf.writeByte(proxyData.getSeq());       // 包名
                buf.writeByte(proxyData.getLastSeverId());  // 服务id
//                buf.writeByte(0);               // 无用
                buf.writeShort(0);              // 无用
                buf.writeInt(9 + jsonBytes.length);          // 数据总长度

                // 以下为代理内容
                buf.writeByte(ProxyType.JSON);                  // 代理类型为json  1
                buf.writeLong(proxyData.getSessionName());      // 连接的别名  8
                buf.writeBytes(jsonBytes);                      // json数据
                ctx.writeAndFlush(buf, promise);
            }
            return;
        }

        if(message instanceof String){
            String msg = ((String) message) + "\n";
            byte[] data = msg.getBytes("UTF8");
            ByteBuf buf = ctx.alloc().buffer(data.length);
            buf.writeBytes(data);       // 数据
            ctx.writeAndFlush(buf, promise);
            return;
        }
    }
}
