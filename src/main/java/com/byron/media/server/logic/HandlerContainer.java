package com.byron.media.server.logic;

import com.byron.media.server.model.MediaHeaderObj;
import com.byron.media.server.sessions.RealSession;
import io.netty.channel.ChannelHandlerContext;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class HandlerContainer {

    private Map<String, IMediaHandler> handlerMap;
    {
        handlerMap = new ConcurrentHashMap<>();
        handlerMap.put("reg", new RegHandler());            // 设备注册
        handlerMap.put("unreg", new UnregHandler());        // 设备注销注册
        handlerMap.put("proxy-reg", new ProxyRegHandler());        // 设备注销注册
        handlerMap.put("proxy-unreg", new ProxyUnregHandler());        // 设备注销注册
    }

    /**
     * 处理流媒体逻辑
     */
    public void handle(String reg, RealSession session, ChannelHandlerContext ctx, MediaHeaderObj header){
        IMediaHandler handler = handlerMap.get(reg);
        if(handler != null){
            handler.handle(session, ctx, header);
        }
    }

    private static HandlerContainer instance;

    public static HandlerContainer getInstance() {
        if (instance == null) {
            synchronized (HandlerContainer.class) {
                if (instance == null) {
                    instance = new HandlerContainer();
                }
            }
        }
        return instance;
    }

}
