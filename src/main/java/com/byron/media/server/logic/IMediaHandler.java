package com.byron.media.server.logic;

import com.byron.media.server.model.MediaHeaderObj;
import com.byron.media.server.sessions.RealSession;
import io.netty.channel.ChannelHandlerContext;

public interface IMediaHandler {

    /**
     * 处理逻辑
     */
    void handle(RealSession session, ChannelHandlerContext ctx, MediaHeaderObj header);


}
