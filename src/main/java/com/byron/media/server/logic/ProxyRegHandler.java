package com.byron.media.server.logic;

import com.byron.media.server.enums.Args;
import com.byron.media.server.model.MediaHeaderObj;
import com.byron.media.server.sessions.ClientSession;
import com.byron.media.server.sessions.ProxyMediaSession;
import com.byron.media.server.sessions.RealSession;
import io.netty.channel.ChannelHandlerContext;
import org.apache.commons.lang.StringUtils;

/**
 * cmd = proxy-reg
 */
public class ProxyRegHandler implements IMediaHandler {

    @Override
    public void handle(RealSession session, ChannelHandlerContext ctx, MediaHeaderObj header) {

        if(header.getArgs() == null){
            return;
        }

        ProxyMediaSession proxySession = new ProxyMediaSession(session.getProxyServer().getServerId(), session);
        proxySession.active(header);
        ctx.fireChannelReadComplete();
    }
}
