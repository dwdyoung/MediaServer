package com.byron.media.server.logic;

import com.byron.media.server.enums.Args;
import com.byron.media.server.model.MediaHeaderObj;
import com.byron.media.server.sessions.ISession;
import com.byron.media.server.sessions.RealSession;
import com.byron.media.server.sessions.SessionContainer;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

// cmd = proxy-unreg
@Slf4j
public class ProxyUnregHandler implements IMediaHandler {

    @Override
    public void handle(RealSession session, ChannelHandlerContext ctx, MediaHeaderObj header) {
        if(header.getArgs() == null){
            return;
        }
        long sessionName = Long.valueOf((String) header.getArgs().get(Args.SESSION_NAME));
//        boolean sender = (boolean) header.getArgs().getOrDefault(Args.SENDER, false);
//        String device = (String) header.getDevice().get("id");

        ISession proxySession = SessionContainer.getInstance().getSessionByName(sessionName);
        if(proxySession != null){
            proxySession.inactive();
        }else {
            log.error("找不到代理连接，连接名字" + sessionName);
        }

        ctx.fireChannelReadComplete();
    }
}
