package com.byron.media.server.logic;

import com.byron.media.server.config.MediaServerConfig;
import com.byron.media.server.enums.Args;
import com.byron.media.server.model.MediaHeaderObj;
import com.byron.media.server.sessions.ClientSession;
import com.byron.media.server.sessions.ProxyServer;
import com.byron.media.server.sessions.RealSession;
import io.netty.channel.ChannelHandlerContext;

/**
 * cmd = reg
 */
public class RegHandler implements IMediaHandler {

    public RegHandler() {
    }

    @Override
    public void handle(RealSession session, ChannelHandlerContext ctx, MediaHeaderObj header) {

        if(header.getArgs() == null){
            return;
        }

//        String groupName = (String) header.getArgs().get(Args.GROUP_NAME);
        boolean proxyServer = (boolean) header.getArgs().getOrDefault(Args.PROXY, false);        // 是否从代理服务器中发送
//        String url = (String)header.getArgs().getOrDefault(Args.RTSP, "");
//        if(StringUtils.isNotEmpty(url)){
//            return;
//        }
//        String device = (String) header.getDevice().get("id");

        // 防止重复注册
        if(session.isActive()){
            return;
        }

        if(!proxyServer){
            ClientSession clientSession = new ClientSession(session);
            clientSession.active(header);
            ctx.fireChannelReadComplete();
            return;
        }


        // 激活为代理服务器
        ProxyServer server = new ProxyServer(session);
        byte serverId = ((Double) header.getArgs().get(Args.SERVER_ID)).byteValue();
        server.active(serverId);


        // 暂不处理一个用户对多个组的情况
//        if(proxy){
//            int serverId = (int) header.getArgs().get(Args.SERVER_ID);
//            if(!session.isProxy()){
//                session.activeAsProxy(serverId);
//            }
//        } else {
//
//        }

    }
}
