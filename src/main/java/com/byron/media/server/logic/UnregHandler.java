package com.byron.media.server.logic;

import com.byron.media.server.model.MediaHeaderObj;
import com.byron.media.server.sessions.ISession;
import com.byron.media.server.sessions.RealSession;
import io.netty.channel.ChannelHandlerContext;

public class UnregHandler implements IMediaHandler {

    @Override
    public void handle(RealSession session, ChannelHandlerContext ctx, MediaHeaderObj header) {
        if(header.getArgs() == null){
            return;
        }
        ISession clientSession = session.getClientSession();
        clientSession.inactive();
        ctx.fireChannelReadComplete();
    }
}
