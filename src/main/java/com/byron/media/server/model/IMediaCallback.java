package com.byron.media.server.model;

public interface IMediaCallback {

    void onData(byte channel, byte type, long ID, byte[] data, int length);
}
