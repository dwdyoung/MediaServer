package com.byron.media.server.model;

import lombok.Data;

@Data
public class MediaData {

    private byte channel;       // 通道号   8 bit

    private byte type;          // 开始位 1、结束位 1、类型 4、保留 2    8 bit

    private int ID;            // 单元号  32 bit

    private byte serverId;     // 服务器id 4 bit

    private byte seq;          // 包名     4 bit

    private short noUse;       // 保留     24 bit 保留

    private int length;         // 数据长度  32 bit

    private byte[] data;        // 数据

    private transient boolean using;      // 是否正在使用

    public MediaData(byte channel, byte type, int ID, byte serverId, byte seq, byte[] data, int length) {
        this.channel = channel;
        this.type = type;
        this.ID = ID;
        this.serverId = serverId;
        this.seq = seq;
        this.data = data;
        this.length = length;
    }

}
