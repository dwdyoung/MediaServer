package com.byron.media.server.model;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class MediaHeaderObj {

    private String head;

    private Map<String, Object> cmd = new HashMap<>();

    private Map<String, Object> device = new HashMap<>();

    private Map<String, Object> args = new HashMap<>();

}
