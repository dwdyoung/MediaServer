package com.byron.media.server.model;

import com.byron.media.server.enums.Args;
import com.byron.media.server.enums.ProxyType;
import com.byron.media.server.utils.JsonUtils;

public class ProxyMediaData {

    private String groupName;       // 组名

    private MediaData mediaData;    // 代理里面的流媒体数据，type为media data有效

    private String json;            // 代理里面的json数据，type为json有效

    private byte proxyType;         // 代理类型

    private long sessionName;     // 消息发送者，连接的名字

    private byte seq;               // 包名

    private byte lastSeverId;          // 服务器名字

    public ProxyMediaData(long sessionName, String groupName, MediaData mediaData) {
        this.sessionName = sessionName;
        this.groupName = groupName;
        this.mediaData = mediaData;
        this.proxyType = ProxyType.MEDIA_DATA;
        this.seq = mediaData.getSeq();
        this.lastSeverId = mediaData.getServerId();
    }

    public ProxyMediaData(long sessionName, byte seq, byte lastSeverId, String json) {
        this.sessionName = sessionName;
        this.proxyType = ProxyType.JSON;
        this.json = json;
        this.seq = seq;
        this.lastSeverId = lastSeverId;
    }

    public ProxyMediaData(byte seq, MediaHeaderObj header) {
        this.sessionName = Long.valueOf((String)header.getArgs().get(Args.SESSION_NAME));
        this.proxyType = ProxyType.JSON;
        this.seq = seq;
        this.json = JsonUtils.toJson(header);
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public MediaData getMediaData() {
        return mediaData;
    }

    public void setMediaData(MediaData mediaData) {
        this.mediaData = mediaData;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public byte getProxyType() {
        return proxyType;
    }

    public void setProxyType(byte proxyType) {
        this.proxyType = proxyType;
    }

    public long getSessionName() {
        return sessionName;
    }

    public void setSessionName(long sessionName) {
        this.sessionName = sessionName;
    }

    public byte getSeq() {
        return seq;
    }

    public byte getLastSeverId() {
        return lastSeverId;
    }
}
