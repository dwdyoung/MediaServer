package com.byron.media.server.model;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 包名生成器
 */
public class SeqFactory {

    private AtomicInteger seq = new AtomicInteger(0);

    /**
     * 生成唯一包名
     * @return
     */
    public byte generalSeq(){
        return (byte) seq.incrementAndGet();
    }

    private static SeqFactory instance;
    public static SeqFactory getInstance(){
        if(instance == null){
            synchronized (SeqFactory.class) {
                if(instance == null){
                    instance = new SeqFactory();
                }
            }
        }
        return instance;
    }

}
