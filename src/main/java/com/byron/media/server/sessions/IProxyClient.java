package com.byron.media.server.sessions;

import com.byron.media.server.model.MediaHeaderObj;

/**
 * 与proxyServer相对，也是连接到代理服务器
 */
public interface IProxyClient {

    /**
     * 激活代理客户端
     */
    void active();

    /**
     * 注销代理客户端
     */
    void inactive();

    /**
     * 发送代理消息
     */
    void write(byte seq, MediaHeaderObj header);
}
