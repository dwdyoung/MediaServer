package com.byron.media.server.sessions;

import com.byron.media.server.model.MediaHeaderObj;

/**
 * 代理服务器
 */
public interface IProxyServer {

    /**
     * 激活代理服务器
     */
    void active(byte serverId);

    /**
     * 取消激活
     */
    void inactive();

    /**
     * 向代理发送消息
     */
    void write(byte seq, MediaHeaderObj header);

    /**
     * 代理服务器的id
     */
    byte getServerId();
}
