package com.byron.media.server.sessions;

import com.byron.media.server.model.MediaData;
import com.byron.media.server.model.MediaHeaderObj;

public interface ISession {

    /**
     * 向该session写出
     */
    void write(byte lastServerId, long fromSessionName, MediaData data);

    /**
     * 该session的设备
     */
    String getDevice();

    /**
     * 该session所在的组
     */
    String getGroupName();


    /**
     * 连接名字（全网唯一且统一）
     * @return
     */
    long getSessionName();

    /**
     * 是否已激活
     */
    boolean isActive();

    /**
     * 激活链接
     */
    void active(MediaHeaderObj header);

    /**
     * 取消激活
     */
    void inactive();

    /**
     * 发送到其他终端
     */
    void sendToOther(MediaData mediaData);
}
