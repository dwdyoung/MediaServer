package com.byron.media.server.sessions;

import com.byron.media.server.model.MediaHeaderObj;
import com.byron.media.server.model.ProxyMediaData;

/**
 * 代理客户端
 */
public class ProxyClient implements IProxyClient{

    private RealSession session;

    public ProxyClient(RealSession session) {
        this.session = session;
    }

    @Override
    public void active() {
        SessionContainer.getInstance().addToProxyClient(this);
    }

    @Override
    public void inactive() {
        SessionContainer.getInstance().removeFromProxyClient(this);
    }

    @Override
    public void write(byte seq, MediaHeaderObj header) {
        if(this.session != null){
            ProxyMediaData data = new ProxyMediaData(seq, header);
            session.write(data);
        }
    }
}
