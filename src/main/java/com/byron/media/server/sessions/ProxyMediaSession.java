package com.byron.media.server.sessions;

import com.byron.media.server.config.MediaServerConfig;
import com.byron.media.server.enums.Args;
import com.byron.media.server.model.MediaData;
import com.byron.media.server.model.MediaHeaderObj;
import com.byron.media.server.model.ProxyMediaData;
import lombok.extern.slf4j.Slf4j;


/**
 * 代理连接
 */
@Slf4j
public class ProxyMediaSession implements ISession {

    // 真实的连接
    private RealSession realSession;

    // 连接的唯一标识
    private String device;

    // 组名字
    private String groupName;

    // 是否为发送端
    @Deprecated
    private boolean sender;

    // 是否已激活
    private boolean active;

    // 代理用的服务器id
    private byte serverId;

    private long sessionName;

    private Group group;

    public ProxyMediaSession(byte serverId, RealSession realSession) {
        this.serverId = serverId;
        this.realSession = realSession;
    }

    @Override
    public void write(byte lastServerId, long fromSessionName, MediaData data) {

        // 如果是从上一站发过来的，则不需要发送
        if(lastServerId == serverId){
            return;
        }

        // 如果是从第一站发出来的，则不需要发送
        if(data.getServerId() == serverId){
            return;
        }
        if(realSession == null){
            return;
        }
        realSession.write(new ProxyMediaData(fromSessionName, group.getGroupName(), data));
    }

    @Override
    public String getDevice() {
        return device;
    }

    @Override
    public String getGroupName() {
        return groupName;
    }

    @Override
    public long getSessionName() {
        return sessionName;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void inactive() {
//        SessionContainer.getInstance().removeFromGroup(sessionName, groupName, this);
        group.removeFromGroup(sessionName, this);
        if(MediaServerConfig.getInstance().isLog()){
            log.info("流媒体客户端 " + device + " 以代理方式，通过服务器(serverId:" + serverId + ") 断开连接！！ ");
        }
    }

    @Override
    public void sendToOther(MediaData mediaData) {
        group.sendToOthers(serverId, sessionName, mediaData);
    }

    @Override
    public void active(MediaHeaderObj header) {
        if(active){
            return;
        }

        String groupName = (String) header.getArgs().get(Args.GROUP_NAME);
        long sessionName = Long.valueOf((String) header.getArgs().get(Args.SESSION_NAME));
//        boolean sender = (boolean) header.getArgs().getOrDefault(Args.SENDER, false);
        String device = (String) header.getDevice().get("id");        // 是否从代理服务器中发送

        this.groupName = groupName;
        this.device = device;
        this.serverId = realSession.getProxyServer().getServerId();
        this.sessionName = sessionName;

        SessionContainer.getInstance().sendLastIframe(groupName, this);
        this.group = SessionContainer.getInstance().addToGroup(sessionName, groupName, this);

//        if(!sender){
//            // TODO 向接受者发送前一张I帧
//        }
        this.active = true;
        if(MediaServerConfig.getInstance().isLog()){
            log.info("流媒体客户端 " + device + " 以代理方式，通过服务器(serverId:" + serverId + ") 连接成功");
        }
    }
}
