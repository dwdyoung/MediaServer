package com.byron.media.server.sessions;

import com.byron.media.server.model.MediaHeaderObj;
import com.byron.media.server.model.ProxyMediaData;

import java.util.HashSet;
import java.util.Set;

public class ProxyServer implements IProxyServer{

    private RealSession session;

    private byte serverId;

    private Set<ProxyMediaSession> proxyMediaSessions = new HashSet<>();

    public ProxyServer(RealSession session) {
        this.session = session;
    }

    @Override
    public void active(byte serverId) {
        this.serverId = serverId;
        SessionContainer.getInstance().addToProxy(this);
        this.session.activeAsProxy(this);
    }

    @Override
    public void inactive() {
        for (ProxyMediaSession proxyMediaSession : proxyMediaSessions) {
            proxyMediaSession.inactive();
        }
        SessionContainer.getInstance().removeFromPoxy(this);
    }

    @Override
    public void write(byte seq, MediaHeaderObj header) {
        if(this.session != null){
            ProxyMediaData data = new ProxyMediaData(seq, header);
            session.write(data);
        }
    }

    @Override
    public byte getServerId() {
        return serverId;
    }
}
