package com.byron.media.server.utils;

public class DataInfo {

    private byte[] data;

    private long pts;

    private long dts;

    private boolean video;

    private int offset;

    private int length;

    private long duration;

    public DataInfo(byte[] data, int offset, int length, long pts, long dts, boolean video, long duration) {
        this.data = data;
        this.offset = offset;
        this.length = length;
        this.pts = pts;
        this.video = video;
        this.dts = dts;
        this.duration = duration;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public long getPts() {
        return pts;
    }

    public void setPts(long pts) {
        this.pts = pts;
    }
    public long getDts() {
        return dts;
    }

    public void setDts(long dts) {
        this.dts = dts;
    }

    public boolean isVideo() {
        return video;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
