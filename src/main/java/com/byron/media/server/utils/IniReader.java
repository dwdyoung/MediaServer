package com.byron.media.server.utils;

import com.byron.media.server.config.MediaServerConfig;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Properties;

@Slf4j
public class IniReader implements Serializable {

    /**
     * 配置文件
     * */
    private HashMap iniItem = new HashMap();

    //    /**
//     * 配置文件中对应的模块名,如[Comunication]
//     */
    private transient String sessionName;

    /**
     * 每个模块中所对应的属性
     */

    private transient Properties properties; // properties配置文件

    public IniReader(String filename) throws IOException {
        FileReader fReader = new FileReader(filename);
        BufferedReader reader = new BufferedReader(fReader);
        read(reader);
        reader.close();
        fReader.close();
    }

    protected void read(BufferedReader reader) throws IOException {
        String line;
        while ((line = reader.readLine()) != null) {
            parseLine(line);
        }
    }

    /**
     * 解析从配置文件中读取到的每一行
     * @param propertiLine             从配置文件中读取到的属性值
     */

    private void parseLine(String propertiLine) {
        propertiLine = propertiLine.trim();
        if(propertiLine.startsWith("#") || propertiLine.startsWith(" #")){
            return;
        }
        if (propertiLine.contains("[")) {
            int headIndex = propertiLine.indexOf("[");
            int footIndex = propertiLine.indexOf("]");
            sessionName = propertiLine.substring(headIndex + 1, footIndex);
            properties = new Properties();
            iniItem.put(sessionName, properties);
        } else if (propertiLine.matches(".*=.*")) {
            if (properties != null) {
                int i = propertiLine.indexOf('=');
                String name = propertiLine.substring(0, i);
                String value = propertiLine.substring(i + 1).trim();
                properties.setProperty(name, value);
                if(MediaServerConfig.getInstance().isLog()){
                    log.info("name:" + name + " value:" + value);
                }
            }
        }
    }

    /**
     * @param section 配置文件中模块名
     * @param name    对应模块中的属性名称
     * @return true表示有对应的测试项, false表示没有对应的测试项
     */
    public boolean getValue(String section, String name) {
        boolean ret = false;
        Properties p = (Properties) iniItem.get(section);
        /**如果没有设置该默认为显示*/
        if (p == null) {
            return false;
        }
        String value = p.getProperty(name);
        ret = ("1".equals(value)) ? true : false;
        return ret;

    }

    public String getValueStr(String section, String name) {
        Properties p = (Properties) iniItem.get(section);
        /**如果没有设置该默认为显示*/
        if (p == null) {
            return null;
        }
        String value = p.getProperty(name);
        return value;
    }
}

