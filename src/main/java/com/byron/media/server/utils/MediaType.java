//package com.byron.media.server.utils;
//
//import com.byron.media.server.model.MediaData;
//
//public class MediaType {
//
//    // type 结构
//    // 开始位(1) 结束位(1) 类型(4) 保留(2)
//    public static final int H264 = 0;
//    public static final int PCM = 1;
//    public static final int AAC = 2;
//    public static final int TEST = 3;
//    public static final int PROXY = 4;
//    public static final int VP8 = 5;
//    public static final int VP9 = 6;
//    public static final int H265 = 7;       // H265格式的视频数据
//    public static final int PDF = 8;        // pdf或者图片的批注
//    public static final int HEART = 9;      // 心跳
//
//
//    /**
//     * 该数据包是否为开始包
//     */
//    public static boolean isStart(byte type){
//        int start = (type >> 7) & 0x01;
//        return start > 0;
//    }
//
//    /**
//     * 该数据包是否为结束包
//     */
//    public static boolean isEnd(byte type){
//        int end = (type >> 6) & 0x01;
//        return end > 0;
//    }
//
//    /**
//     * 该数据包是否H264
//     */
//    public static boolean isH264(byte type){
//        int t = (type >> 2) & 0x0f;
//        return t == H264;
//    }
//
//    /**
//     * 该数据包是否为PCM
//     */
//    public static boolean isPCM(byte type){
//        int t = (type >> 2) & 0x0f;
//        return t == PCM;
//    }
//
//    /**
//     * 是否是测试数据
//     */
//    public static boolean isTest(byte type){
//        int t = (type >> 2) & 0x0f;
//        return t == TEST;
//    }
//
//    public static boolean isAAC(byte type){
//        int t = (type >> 2) & 0x0f;
//        return t == AAC;
//    }
//
//    public static boolean isHeart(byte type){
//        int t = (type >> 2) & 0x0f;
//        return t == HEART;
//    }
//
//    /**
//     * 设置开始位
//     */
//    public static byte putStart(byte type){
//        return (byte) (type | 0x80);
//    }
//
//    /**
//     * 设置结束位
//     */
//    public static byte putEnd(byte type){
//        return (byte) (type | 0x40);
//    }
//
//    /**
//     * 设置H264类型
//     */
//    public static byte putH264(byte type){
//        return (byte) ((type & 0xC3) | 0x00);
//    }
//
//    public static byte putPCM(byte type){
//        return (byte) ((type & 0xC3) | 0x04);
//    }
//
//    public static byte putAAC(byte type){
//        return (byte) ((type & 0xC3) | 0x08);
//    }
//
//    public static byte putTest(byte type){
//        return (byte) ((type & 0xC3) | 0x0C);
//    }
//
//    /**
//     * 判断是否是I帧
//     */
//    public static boolean isIframe(MediaData data) {
//        if(!isStart(data.getType())){
//            return false;
//        }
//        if(!isH264(data.getType())){
//            return false;
//        }
//
//        int startIndex = 0;
//        for(int i = 0; i < data.getData().length; i++){
//            if(data.getData()[i] == 0){
//                continue;
//            }
//            startIndex = i;
//            break;
//        }
//
//        if(startIndex > 4){
//            return false;
//        }
//
//        byte naluType = data.getData()[startIndex + 1];
//        naluType = (byte) (naluType & 0x1F);
//        return naluType == 7;
//    }
//
//}
