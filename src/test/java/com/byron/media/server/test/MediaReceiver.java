//package com.byron.media.server.test;
//
//
//import com.byron.media.server.ffmpeg.DataListener;
//import com.byron.media.server.ffmpeg.IFrameListener;
//import com.byron.media.server.ffmpeg.convertor.AnyToYuv;
//import com.byron.media.server.ffmpeg.convertor.YuvToAny;
//import com.byron.media.server.test.receiver.TcpReceiver;
//import com.byron.media.server.utils.DataInfo;
//import com.byron.media.server.enums.MediaType;
//import lombok.extern.slf4j.Slf4j;
//import org.bytedeco.javacpp.avutil;
//import org.junit.Test;
//
//@Slf4j
//public class MediaReceiver {
//
//    @Test
//    public void testReceiver() throws InterruptedException {
//
//        final AnyToYuv anyToYuv = new AnyToYuv(MediaType.VP8);
//        anyToYuv.initDecoder();
//
//        final YuvToAny yuvToAny = new YuvToAny(MediaType.H264);
//        yuvToAny.initEncoder();
//
//        anyToYuv.setFrameListener(new IFrameListener() {
//            @Override
//            public void onFrame(avutil.AVFrame frame) {
//                yuvToAny.push(frame);
//            }
//        });
//
//        yuvToAny.setDataListener(new DataListener() {
//            @Override
//            public void onFrame(DataInfo dataInfo) {
//                log.info("dddd");
//            }
//        });
//
//        TcpReceiver receiver = new TcpReceiver("127.0.0.1", 1553, "239.255.255.1:1901");
//        receiver.setDataListener(new DataListener() {
//            @Override
//            public void onFrame(DataInfo dataInfo) {
//                byte[] data = new byte[dataInfo.getLength()];
//                System.arraycopy(dataInfo.getData(), 0, data, 0, dataInfo.getLength());
//                anyToYuv.pushFrame(data, data.length);
//            }
//        });
//        receiver.start();
//
//        Thread.sleep(999999999L);
//    }
//}
