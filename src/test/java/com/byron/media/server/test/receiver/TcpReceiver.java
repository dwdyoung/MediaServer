//package com.byron.media.server.test.receiver;
//
//
//import com.byron.media.server.utils.CommonUlit;
//import com.byron.media.server.utils.DataInfo;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.net.InetSocketAddress;
//import java.net.Socket;
//import java.net.SocketException;
//
///**
// * Created by monday on 2017/9/3.
// */
//
//public class TcpReceiver extends Thread {
//
//    private Socket socket;
//
//    private InputStream inputStream;
//
//    private OutputStream outputStream;
//
//    private String serverAddress;
//
//    private int serverPort;
//
//    private boolean running = true;        // 线程运行
//
//    private String groupName;
//
//    private DataListener dataListener;
//
//    private byte[] bigBuffer;           // 缓存空间
//
//    private int bufferedPosition = 0;       // 缓存中指针位置
//
//    public TcpReceiver(String serverAddress, int serverPort, String groupName) {
//        this.serverAddress = serverAddress;
//        this.serverPort = serverPort;
//        this.groupName = groupName;
//        this.bigBuffer = new byte[1024 * 5000];
//    }
//
//    public void init(){
//        try {
//            socket = new Socket();
//            socket.connect(new InetSocketAddress(serverAddress, serverPort));
//            inputStream = socket.getInputStream();
//            outputStream = socket.getOutputStream();
//
//            String message = "{\"head\":\"gonsin\"," +
//            "\"device\":{\"id\":\"" + 9898 + "\"}," +
//                    "\"cmd\":{\"cmd\":\"reg\",\"respone\":0}," +
//                    "\"args\":{\"group_name\":\"" + groupName + "\"}}\n";
//
//            outputStream.write(message.getBytes());
//            outputStream.flush();
//
//        } catch (SocketException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void run() {
//
//        init();
//
//        while (!Thread.interrupted() && running) {
//
//            // 读取udp数据
//            try {
//
//                int available = 0;
//                while (available < 15 && running){
//                    available = inputStream.available();
//                }
//                byte[] head = new byte[15];
//                inputStream.read(head);
//
//                byte channel = head[1];
//                byte bit = head[2];
//                long id = CommonUlit.getLong(head, 3);
//                int length = CommonUlit.getMinaInt(head, 11);
//                while (available < length && running){
//                    available = inputStream.available();
//                }
//
//                boolean start = (((bit >> 7) & 0x01) == 1);
//                boolean end = (((bit >> 6) & 0x01) == 1) ;
//                int type = ((bit >> 2) & 0x0F);
//
//                while (available < length){
//                    available = inputStream.available();
//                }
//
//                byte[] data = new byte[length];
//                inputStream.read(data);
//
//                if(start && end){
//                    if(dataListener != null){
//                        dataListener.onFrame(new DataInfo(data, 0, length, 0, 0, true, 0));
//                    }
//                    continue;
//                }
//
//                for(int i = 0; i < length; i++){
//                    bigBuffer[i + bufferedPosition] = data[i];
//                }
//                bufferedPosition += length;
//
//                if(end){
//                    if(dataListener != null) {
//                        dataListener.onFrame(new DataInfo(bigBuffer, 0, bufferedPosition, 0, 0, true, 0));
//                    }
//                    bufferedPosition = 0;
//                }
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//        inputStream = null;
//        socket = null;
//        bigBuffer = null;
//        outputStream = null;
//    }
//
//    public void close(){
//        running = false;
//
//        if(outputStream != null){
//            try {
//                outputStream.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//        if(inputStream != null){
//            try {
//                inputStream.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//        if(socket != null){
//            try {
//                socket.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//    }
//
//
//    public void setDataListener(DataListener dataListener) {
//        this.dataListener = dataListener;
//    }
//
//
//}
//
