//package com.byron.media.server.test.rtsp;
//
//import com.byron.media.server.config.MediaServerConfig;
//import com.byron.media.server.enums.MediaType;
//import com.byron.media.server.ffmpeg.AnyReceiver;
//import com.byron.media.server.ffmpeg.DataListener;
//import com.byron.media.server.rtsp.sender.MediaSender;
//import com.byron.media.server.utils.DataInfo;
//import lombok.SneakyThrows;
//import org.apache.commons.io.FileUtils;
//import org.junit.Test;
//
//import java.io.File;
//
//public class RtspPullerTest {
//
//    @Test
//    public void test() throws InterruptedException {
//        String rtsp = "rtsp://192.168.2.88/hdmi";
//        String mediaServer = "127.0.0.1";
//        String group = "239.255.255.1:9999";
//        int mediaPort = 1553;
//
//        MediaServerConfig config = new MediaServerConfig();
//        MediaSender sender = new MediaSender(mediaServer, mediaPort, group, (byte)0, 0, MediaType.H264);
//        sender.start();
//
//        AnyReceiver receiver = new AnyReceiver(config, 1280, 800, rtsp, true,
//                new DataListener() {
//            @Override
//            public void onFrame(DataInfo dataInfo) {
////                sender.pushFrameInfo(dataInfo);
//                writeToFile(dataInfo);
//            }
//        });
//        receiver.start();
//
//        Thread.sleep(10000000l);
//
//    }
//
//
//
//    private File h264File;
//    @SneakyThrows
//    private void writeToFile(DataInfo dataInfo) {
//        if(h264File == null){
//            h264File = new File("temp.h264");
//            if(h264File.exists()){
//                h264File.delete();
//            }
//            h264File.createNewFile();
//        }
//
//        FileUtils.writeByteArrayToFile(h264File, dataInfo.getData(),0, dataInfo.getLength(), true);
//    }
//
//}
